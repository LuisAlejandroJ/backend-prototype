package com.payu.app.network.dto;

import com.payu.app.domain.Order;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class BuyerDto {

    private String fullName;

    private String emailAddress;

    private String contactPhone;

    private String dniNumber;

    private AddressDto shippingAddress;

    public static BuyerDto from(Order order){
        var shippingAddress = AddressDto
                .from(order.getShippingAddress());
        var buyer = order.getBuyer();
        return BuyerDto
                .builder()
                .withFullName(buyer.getName())
                .withEmailAddress(buyer.getEmail())
                .withContactPhone(buyer.getContactPhone())
                .withDniNumber(buyer.getDniNumber())
                .withShippingAddress(shippingAddress)
                .build();

    }

}
