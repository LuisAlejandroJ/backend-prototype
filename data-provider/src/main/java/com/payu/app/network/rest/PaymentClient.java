package com.payu.app.network.rest;

import com.payu.app.network.dto.PaymentRequestDto;
import com.payu.app.network.dto.PaymentResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url = "https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi", name = "payment")
public interface PaymentClient {

    @PostMapping
    PaymentResponseDto pay(@RequestBody PaymentRequestDto payment);
}
