package com.payu.app.network.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.payu.app.domain.CreditCard;
import lombok.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class CreditCardDto {

    private String number;

    private String securityCode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM")
    private Date expirationDate;

    private String name;

    private Boolean processWithoutCvv2;

    public static CreditCardDto from(CreditCard creditCard){

        return CreditCardDto
                .builder()
                .withNumber(creditCard.getCardNumber())
                .withSecurityCode(creditCard.getSecurityCode())
                .withExpirationDate(creditCard.getExpirationDate())
                .withName(creditCard.getPayerName())
                .withProcessWithoutCvv2(false)
                .build();
    }

}
