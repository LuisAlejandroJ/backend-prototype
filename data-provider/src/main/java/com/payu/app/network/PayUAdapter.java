package com.payu.app.network;

import com.payu.app.domain.*;
import com.payu.app.network.dto.PaymentRequestDto;
import com.payu.app.network.dto.interceptor.PaymentRequestInterceptor;
import com.payu.app.network.dto.interceptor.PaymentResponseInterceptor;
import com.payu.app.network.rest.PaymentClient;
import com.payu.app.repository.PaymentEngineAdapter;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class PayUAdapter implements PaymentEngineAdapter {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    PaymentClient paymentClient;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public Optional<Transaction> pay(Order order, CreditCard paymentCard) {

        var paymentResponse = paymentClient.pay(PaymentRequestDto.from(order, paymentCard));
        return PaymentResponseInterceptor.intercept(paymentResponse,order,paymentCard);
    }

    @Override
    public Optional<Transaction> pay(Order order, TokenizeCard paymentCard) {
        return Optional.empty();
    }

    @Override
    public Optional<Transaction> refundOrder(Transaction transaction) {
        return Optional.empty();
    }

    @Override
    public Optional<TokenizeCard> tokenizeCard(CreditCard card, User user) {
        return Optional.empty();
    }

    @Override
    public Optional<Order> consultOrder(String orderId) {
        return Optional.empty();
    }

}
