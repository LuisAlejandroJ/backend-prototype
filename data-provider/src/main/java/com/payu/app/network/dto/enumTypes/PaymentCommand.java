package com.payu.app.network.dto.enumTypes;

public enum PaymentCommand {
    SUBMIT_TRANSACTION, CANCEL_TRANSACTION ;

}
