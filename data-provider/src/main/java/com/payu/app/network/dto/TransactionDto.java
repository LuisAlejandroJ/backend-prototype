package com.payu.app.network.dto;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.Order;
import com.payu.app.domain.enumTypes.PaymentMethod;
import com.payu.app.network.dto.enumTypes.TransactionType;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class TransactionDto {

    private OrderDto order;

    private CreditCardDto creditCard;

    private PayerDto payer;

    private TransactionType type; // ???????

    private PaymentMethod paymentMethod; // ????

    private String paymentCountry;

    private String deviceSessionId;

    private String ipAddress;

    private String cookie;

    private String userAgent;

    public static TransactionDto from(Order order, CreditCard creditCard) {

        var creditCardReq = CreditCardDto.from(creditCard);

        var orderReq = OrderDto.from(order);

        return TransactionDto
                .builder()
                .withOrder(orderReq)
                .withCreditCard(creditCardReq)
                .withType(TransactionType.AUTHORIZATION_AND_CAPTURE)
                .withPaymentMethod(creditCard.getPaymentMethod())
                .withPaymentCountry("CO")
                .withDeviceSessionId(order.getDeviceSessionId())
                .withIpAddress(order.getIpAddress())
                .withCookie(order.getCookie())
                .withUserAgent(order.getUserAgent())
                .build();

    }
}
