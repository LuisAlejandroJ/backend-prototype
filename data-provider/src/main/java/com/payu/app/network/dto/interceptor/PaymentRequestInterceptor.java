package com.payu.app.network.dto.interceptor;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.Order;
import com.payu.app.network.dto.*;
import com.payu.app.network.dto.enumTypes.PaymentCommand;
import com.payu.app.network.dto.enumTypes.TransactionType;

public class PaymentRequestInterceptor {

    private final static String CURRENCY = "ARS";

    public static PaymentRequestDto intercept(Order order, CreditCard card) {

        var orderValue = order.calculateTotalAmount();

        var merchantReq = new MerchantDto("4Vj8eK4rloUd272L48hsrarnUA",
                "pRRXKOl8ikMmt9u");

        var additionalValueReq = new AdditionalValuesDto(
                new TxAdditionalValueDto(orderValue, CURRENCY)
                , new TxAdditionalValueDto(0.0, CURRENCY)
                , new TxAdditionalValueDto(0.0, CURRENCY));

        var addressReq = new AddressDto(
                order.getShippingAddress().getStreetAddress()
                , order.getShippingAddress().getCity()
                , order.getShippingAddress().getCountry()
        );

        var buyerReq = new BuyerDto(order.getBuyer().getName()
                , order.getBuyer().getEmail()
                , order.getBuyer().getContactPhone()
                , order.getBuyer().getDniNumber()
                , addressReq);

        var payerReq = new PayerDto(order.getBuyer().getEmail()
                , order.getBuyer().getName()
                , addressReq
                , order.getBuyer().getContactPhone()
                , order.getBuyer().getDniNumber());

        var orderReq = new OrderDto("512321"
                , "TestPayU"
                , "payment test"
                , "es"
                , "7ee7cf808ce6a39b17481c54f2c57acc"
                , buyerReq
                , additionalValueReq);

        var creditCardReq = new CreditCardDto(card.getCardNumber()
                , card.getSecurityCode()
                , card.getExpirationDate()
                , card.getPayerName()
                , false);

        var transactionReq = new TransactionDto(orderReq
                , creditCardReq
                , payerReq
                , TransactionType.AUTHORIZATION_AND_CAPTURE
                , card.getPaymentMethod()
                , "CO"
                , order.getDeviceSessionId()
                , order.getIpAddress()
                , order.getCookie()
                , order.getUserAgent());

        return new PaymentRequestDto("es"
                , PaymentCommand.SUBMIT_TRANSACTION
                , true
                , merchantReq
                , transactionReq);
    }

}
