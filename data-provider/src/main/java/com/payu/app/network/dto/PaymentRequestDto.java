package com.payu.app.network.dto;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.Order;
import com.payu.app.network.dto.enumTypes.PaymentCommand;
import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class PaymentRequestDto implements Serializable {

    private String language;

    private PaymentCommand command; // [ submit_transaction ]

    private Boolean test;

    private MerchantDto merchant;

    private TransactionDto transaction;


    public static PaymentRequestDto from(Order order, CreditCard creditCard) {

        return PaymentRequestDto
                .builder()
                .withLanguage("es")
                .withCommand(PaymentCommand.SUBMIT_TRANSACTION)
                .withTest(true)
                .withMerchant(MerchantDto.from("", ""))
                .withTransaction(TransactionDto.from(order, creditCard))
                .build();
    }

}
