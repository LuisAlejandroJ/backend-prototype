package com.payu.app.network.dto;

import com.payu.app.domain.Address;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class AddressDto {

    private String street1;

    private String city;

    private String country;

    public static AddressDto from(Address address){
        return AddressDto
                .builder()
                .withStreet1(address.getStreetAddress())
                .withCity(address.getCity())
                .withCountry(address.getCountry())
                .build();
    }

}
