package com.payu.app.network.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MerchantDto {

    private String apiLogin;

    private String apiKey;

    public static MerchantDto from(String apiLogin, String apiKey) {

        return new MerchantDto(apiLogin, apiKey);
    }
}
