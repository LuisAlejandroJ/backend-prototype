package com.payu.app.network.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TxAdditionalValueDto {

    private Double value;

    private String currency;

    public static TxAdditionalValueDto from (Double value, String currency){
        return new TxAdditionalValueDto(value,currency);
    }

}
