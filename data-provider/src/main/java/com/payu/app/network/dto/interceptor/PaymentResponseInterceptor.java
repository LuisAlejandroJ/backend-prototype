package com.payu.app.network.dto.interceptor;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.Order;
import com.payu.app.domain.Transaction;
import com.payu.app.domain.enumTypes.TransactionStatus;
import com.payu.app.domain.enumTypes.TransactionType;
import com.payu.app.network.dto.PaymentResponseDto;

import java.util.Optional;
import java.util.UUID;

public class PaymentResponseInterceptor {

    private PaymentResponseInterceptor() {

    }

    public static Optional<Transaction> intercept(PaymentResponseDto paymentResponse, Order order, CreditCard card){

        var newTransaction = new Transaction(UUID.randomUUID().toString(),
                getStatus(paymentResponse)
                ,TransactionType.PAYMENT
                ,order.getBuyer()
                ,card
                ,null
                ,paymentResponse.getTransactionResponse().getTransactionId()
                ,paymentResponse.getTransactionResponse().getOrderId());


        var otherTransaction = Transaction
                .builder()
                .withStatus(getStatus(paymentResponse))
                .withId(UUID.randomUUID().toString())
                .build();

        return Optional.of(newTransaction);
    }

    private static TransactionStatus getStatus(PaymentResponseDto paymentResponse){

        if(paymentResponse.getCode().equals("ERROR")){
            return TransactionStatus.DECLINE;
        }else{
            if(paymentResponse.getTransactionResponse().getState().equals("DECLINED")){
                return TransactionStatus.DECLINE;
            }else{
                return TransactionStatus.APPROVED;
            }
        }
    }
}
