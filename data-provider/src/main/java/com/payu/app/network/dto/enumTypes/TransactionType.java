package com.payu.app.network.dto.enumTypes;

public enum TransactionType {
    AUTHORIZATION_AND_CAPTURE,AUTHORIZATION,CAPTURE;
}
