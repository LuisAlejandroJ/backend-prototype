package com.payu.app.network.dto;

import com.payu.app.domain.Order;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class AdditionalValuesDto {

    private TxAdditionalValueDto TX_VALUE;

    private TxAdditionalValueDto TX_TAX;

    private TxAdditionalValueDto TX_TAX_RETURN_BASE;

    public static AdditionalValuesDto from(Order order){

        var CURRENCY = "COP";

        return AdditionalValuesDto
                .builder()
                .withTX_VALUE(
                        TxAdditionalValueDto
                                .from(order.calculateTotalAmount(),CURRENCY)
                )
                .withTX_TAX(
                        TxAdditionalValueDto.from(0.0,CURRENCY)
                )
                .withTX_TAX_RETURN_BASE(
                        TxAdditionalValueDto.from(0.0,CURRENCY)
                )
                .build();
    }


}
