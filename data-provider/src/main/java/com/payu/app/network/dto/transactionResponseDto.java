package com.payu.app.network.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class transactionResponseDto {

    private String orderId;

    private String transactionId;

    private String state;

    private String paymentNetworkResponseCode;

    private String paymentNetworkResponseErrorMessage;

    private String trazabilityCode;

    private String authorizationCode;

    private String pendingReason;

    private String responseCode;

    private String errorCode;

    private String responseMessage;

    private String transactionDate;

    private String transactionTime;

    private String operationDate;

    private String extraParameters;

}
