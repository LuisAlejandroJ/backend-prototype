package com.payu.app.network.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PayerDto {

    private String emailAddress;

    private String fullName;

    private AddressDto billingAddress;

    private String contactPhone;

    private String dniNumber;

}
