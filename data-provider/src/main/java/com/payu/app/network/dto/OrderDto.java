package com.payu.app.network.dto;

import com.payu.app.domain.Order;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
public class OrderDto {

    private String accountId;

    private String referenceCode;

    private String description;

    private String language;

    private String signature;

    private BuyerDto buyer;

    private AdditionalValuesDto additionalValues;

    public static OrderDto from(Order order){

        var buyer = BuyerDto.from(order);

        var additionalValues = AdditionalValuesDto.from(order);

        return OrderDto
                .builder()
                .withAccountId("512321")
                .withReferenceCode("Test Payu")
                .withDescription("Payment test")
                .withLanguage("es")
                .withSignature("7ee7cf808ce6a39b17481c54f2c57acc")
                .withBuyer(buyer)
                .withAdditionalValues(additionalValues)
                .build();
    }

}
