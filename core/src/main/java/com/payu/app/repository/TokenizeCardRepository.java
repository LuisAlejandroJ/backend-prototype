package com.payu.app.repository;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.TokenizeCard;

import java.util.Collection;
import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface TokenizeCardRepository {

    Optional<TokenizeCard> add(CreditCard creditCard);

    Collection<TokenizeCard> getByEmail(String email);

}
