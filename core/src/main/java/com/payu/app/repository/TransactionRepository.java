package com.payu.app.repository;

import com.payu.app.domain.Transaction;

import java.util.Collection;
import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface TransactionRepository {

    Optional<Transaction> add(Transaction transaction);

    Collection<Transaction> getAll();

    Optional<Transaction> getByOrderId(String orderId);

}
