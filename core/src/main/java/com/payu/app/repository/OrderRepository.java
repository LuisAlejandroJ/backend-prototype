package com.payu.app.repository;

import com.payu.app.domain.Order;

import java.util.Collection;
import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface OrderRepository {

    Optional<Order> add(Order order);

    Optional<Order> update(String id, Order order);

    Collection<Order> getByEmail(String email);

    Optional<Order> getById(String id);
}
