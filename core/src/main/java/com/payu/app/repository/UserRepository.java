package com.payu.app.repository;

import com.payu.app.domain.User;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface UserRepository {

    Optional<User> add(User user);

    Optional<User> getUserByEmail(String email);

}
