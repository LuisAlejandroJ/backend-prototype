package com.payu.app.repository;

import com.payu.app.domain.*;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface PaymentEngineAdapter {

    Optional<Transaction> pay(Order order, CreditCard paymentCard);

    Optional<Transaction> pay(Order order, TokenizeCard paymentCard);

    Optional<Transaction> refundOrder(Transaction transaction);

    Optional<TokenizeCard> tokenizeCard(CreditCard card, User user);

    Optional<Order> consultOrder(String orderId);

}
