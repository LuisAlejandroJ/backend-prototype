package com.payu.app.repository;

import com.payu.app.domain.Product;

import java.util.Collection;
import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public interface ProductRepository {

    Optional<Product> add(Product product);

    Optional<Product> delete(String id);

    Collection<Product> getAll();

    Optional<Product> getById(String id);

    Optional<Product> update(String id,Product product);

}
