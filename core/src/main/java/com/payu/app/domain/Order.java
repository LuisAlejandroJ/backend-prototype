package com.payu.app.domain;

import com.payu.app.domain.enumTypes.OrderStatus;
import com.payu.app.domain.exception.InvalidOrderException;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    //UUID
    private String id;

    private User buyer;

    private Date orderDate;

    private Address shippingAddress;

    private OrderStatus status;

    private Collection<Transaction> transactions;

    private Collection<Product> products;

    /**
     * Need this at the Payment Engine Request
     */

    private String deviceSessionId;

    private String ipAddress;

    private String cookie;

    private String userAgent;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * calculateTotalAmount of {@link Order}
     *
     * @return the total amount of the {@link Order}
     */
    public double calculateTotalAmount() {

        return products.parallelStream()
                .filter(product -> Objects.nonNull(product.getPrice()))
                .mapToInt(product -> product.getQuantity() * product.getPrice())
                .sum();
    }

    /**
     * Change the status of {@link Order}, the status can be changed if it
     * satisfies the business rules,  otherwise a {@link Exception}
     * is gonna be thrown.
     *
     * @param newStatus of the {@link Order}
     */
    public void changeStatus(OrderStatus newStatus) {

        OrderStatus.validateStatusChange(this.status, newStatus);
        this.status = newStatus;
    }

    /**
     * Validate if a new {@link Order} is valid, with {@link Product} collection and with
     * {@link OrderStatus} 'Received', otherwise a {@link InvalidOrderException} is
     * gonna be thrown.
     *
     * @param order instance which is going to be validated.
     */
    public final void isValid(Order order) {
        if (order.getProducts().isEmpty() || order.getStatus() != OrderStatus.RECEIVED) {
            var message = String
                    .format("You can't add a order without products or with status different to 'Received'- Message: Order could not be created. Order id: [%s]", id);
            throw new InvalidOrderException(message);
        }
    }

    public void addTransaction(Transaction transaction) {

        if (Objects.nonNull(transactions)) {
            transactions.add(transaction);
        } else {
            transactions = new ArrayList<>();
            transactions.add(transaction);
        }
    }
    //addTransaction

}