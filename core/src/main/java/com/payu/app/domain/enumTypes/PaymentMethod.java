package com.payu.app.domain.enumTypes;

import com.payu.app.domain.exception.InvalidPaymentMethodException;

public enum PaymentMethod {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    VISA, MASTERCARD, AMEX;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * This method validate if the {@link PaymentMethod} correspond to the
     * number of the card, otherwise a {@link InvalidPaymentMethodException}
     * is gonna be thrown
     *
     * @param maskedNumber of the card
     * @param method the {@link PaymentMethod} of the card
     */
    public static void isValid(String maskedNumber, PaymentMethod method) {
        switch (method) {
            case VISA:
                validateVisaCard(maskedNumber);
                break;
            case MASTERCARD:
                validateMasterCCard(maskedNumber);
                break;
            case AMEX:
                validateAmexCard(maskedNumber);
                break;
        }

    }

    /**
     * Validates if the masked number corresponds to a
     * {@link PaymentMethod#VISA} credit card
     *
     * @param maskedNumber of the card
     */
    private static void validateVisaCard(String maskedNumber) {

        if (maskedNumber.charAt(0) != '4') {
            var message = String.format("Does not correspond to a Visa credit card Message: Invalid Payment method. Masked number: [%s]", maskedNumber);
            throw new InvalidPaymentMethodException(message);
        }
    }

    /**
     * Validates if the masked number corresponds to a
     * {@link PaymentMethod#AMEX} credit card
     *
     * @param maskedNumber of the card
     */
    private static void validateAmexCard(String maskedNumber) {

        if (maskedNumber.charAt(0) != '3') {
            var message = String.format("Does not correspond to a American Express credit card Message: Invalid Payment method. Masked number: [%s]", maskedNumber);
            throw new InvalidPaymentMethodException(message);
        }
    }

    /**
     * Validates if the masked number corresponds to a
     * {@link PaymentMethod#MASTERCARD} credit card
     *
     * @param maskedNumber of the card
     */
    private static void validateMasterCCard(String maskedNumber) {

        if (maskedNumber.charAt(0) != '2' || maskedNumber.charAt(0) != '5') {
            var message = String.format("Does not correspond to a Master Card credit card Message: Invalid Payment method. Masked number: [%s]", maskedNumber);
            throw new InvalidPaymentMethodException(message);
        }
    }

}
