package com.payu.app.domain;

import com.payu.app.domain.enumTypes.UserRol;
import com.payu.app.domain.exception.InvalidUserException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.regex.Pattern;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private String email;

    private String name;

    private String password;

    private String dniNumber;

    private String contactPhone;

    private UserRol rol;

    private Collection<TokenizeCard> tokenizedCards;

    // private Collection<Order> orders;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------


    /**
     * This method validate if the email of {@link User} is valid, otherwise  a
     * {@link InvalidUserException} is gonna be thrown.
     */
    public void isEmailValid() {
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        if (!pattern.matcher(email).find()) {
            var message = String.format("Invalid user - Message: Invalid Email. User email: [%s]", email);
            throw new InvalidUserException(message);
        }
    }

}
