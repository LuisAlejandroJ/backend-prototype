package com.payu.app.domain.enumTypes;

import com.payu.app.domain.exception.InvalidOrderStatusException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public enum OrderStatus {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    RECEIVED, PAYED, UNDERPAYMENT, SHIPPING, REFUNDED, REFUND_REQUEST, DELIVERED;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * This method validate if the change of the {@link OrderStatus} satisfies the
     * business rules, otherwise a {@link InvalidOrderStatusException} is gonna
     * be thrown.
     *
     * @param currentStatus the current {@link OrderStatus}
     * @param newStatus the possible new {@link OrderStatus}
     */
    public static void validateStatusChange(OrderStatus currentStatus, OrderStatus newStatus) {

        switch (currentStatus) {
            case RECEIVED:
                newStatusShouldBePayedOrUnderpayment(newStatus);
                break;
            case PAYED:
                newStatusShouldBeShippingOrRefundRequest(newStatus);
                break;
            case REFUND_REQUEST:
                newStatusShouldBeShippingOrRefunded(newStatus);
                break;
            case SHIPPING:
                newStatusShouldBeDelivered(newStatus);
                break;
            default:
                throw new InvalidOrderStatusException(currentStatus,newStatus);
        }
    }

    /**
     * The new  {@link OrderStatus} should be {@link OrderStatus#PAYED} or
     * {@link OrderStatus#UNDERPAYMENT}, otherwise a
     * {@link InvalidOrderStatusException} is gonna be thrown.
     *
     * @param newStatus the possible new {@link OrderStatus}
     */
    private static void newStatusShouldBePayedOrUnderpayment(OrderStatus newStatus) {

        var isNewStatusValid = newStatus == PAYED || newStatus == UNDERPAYMENT;

        if (!isNewStatusValid) {
            throw new InvalidOrderStatusException(RECEIVED, newStatus);
        }

    }

    /**
     * The new  {@link OrderStatus} should be {@link OrderStatus#SHIPPING} or
     * {@link OrderStatus#REFUND_REQUEST}, otherwise a
     * {@link InvalidOrderStatusException} is gonna be thrown.
     *
     * @param newStatus the possible new {@link OrderStatus}
     */
    private static void newStatusShouldBeShippingOrRefundRequest(OrderStatus newStatus) {

        var isNewStatusValid = newStatus == SHIPPING || newStatus == REFUND_REQUEST;

        if (!isNewStatusValid) {
            throw new InvalidOrderStatusException(PAYED, newStatus);
        }
    }

    /**
     * The new  {@link OrderStatus} should be {@link OrderStatus#SHIPPING} or
     * {@link OrderStatus#REFUNDED}, otherwise a
     * {@link InvalidOrderStatusException} is gonna be thrown.
     *
     * @param newStatus the possible new {@link OrderStatus}
     */
    private static void newStatusShouldBeShippingOrRefunded(OrderStatus newStatus) {

        var isNewStatusValid = newStatus == SHIPPING || newStatus == REFUNDED;

        if (!isNewStatusValid) {
            throw new InvalidOrderStatusException(REFUND_REQUEST, newStatus);
        }
    }

    /**
     * The new  {@link OrderStatus} should be {@link OrderStatus#DELIVERED},
     * otherwise a {@link InvalidOrderStatusException} is gonna be thrown.
     *
     * @param newStatus the possible new {@link OrderStatus}
     */
    private static void newStatusShouldBeDelivered(OrderStatus newStatus) {

        var isNewStatusValid = newStatus == DELIVERED;

        if (!isNewStatusValid) {
            throw new InvalidOrderStatusException(SHIPPING, newStatus);
        }
    }

}
