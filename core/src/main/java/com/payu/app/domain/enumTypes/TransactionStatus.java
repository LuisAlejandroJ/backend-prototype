package com.payu.app.domain.enumTypes;

/**
 * Author: Luis Alejandro Jaramillo
 */
public enum TransactionStatus {

    APPROVED, DECLINE, PENDING;
}
