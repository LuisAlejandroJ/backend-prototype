package com.payu.app.domain;

import com.payu.app.domain.enumTypes.PaymentMethod;
import com.payu.app.domain.exception.InvalidPaymentMethodException;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public abstract class PaymentCard {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private String cardNumber;

    private PaymentMethod paymentMethod;


    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Validate if the {@link PaymentMethod}
     */
    public void isValid() {
        var maskedNumber = cardNumberToMaskedNumber();
        PaymentMethod.isValid(maskedNumber, paymentMethod);

    }

    /**
     * Receive the card number and transforms into a masked number
     *
     * @return masked number of the card
     */
    private String cardNumberToMaskedNumber() {
        if (cardNumber.length() == 16) {
            return String.format("%s******%s", cardNumber.substring(0, 6), cardNumber.substring(11, 16));
        } else {
            var message = String.format("Invalid card Message: The card number is invalid. First 4 numbers: [%s]", cardNumber.substring(0, 4));
            throw new InvalidPaymentMethodException(message);
        }
    }


}
