package com.payu.app.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.payu.app.domain.enumTypes.PaymentMethod;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
public class CreditCard extends PaymentCard {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * private int number;
     * private PaymentMethod paymentMethod;
     */

    /**
     * If the user want to save the card, the value es true, otherwise is false
     */
    private Boolean saveCard;

    private String securityCode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM")
    private Date expirationDate;

    private String payerName;

    public CreditCard(String cardNumber, PaymentMethod paymentMethod, Boolean saveCard, String securityCode, Date expirationDate, String payerName) {
        super(cardNumber, paymentMethod);
        this.saveCard = saveCard;
        this.securityCode = securityCode;
        this.expirationDate = expirationDate;
        this.payerName = payerName;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------


}
