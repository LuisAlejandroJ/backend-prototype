package com.payu.app.domain;

import com.payu.app.domain.enumTypes.PaymentMethod;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
public class TokenizeCard extends PaymentCard{

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private String cardTokenId;

    public TokenizeCard(String cardNumber, PaymentMethod paymentMethod, String cardTokenId) {
        super(cardNumber, paymentMethod);
        this.cardTokenId = cardTokenId;
    }


    // private String maskedNumber;

    // private PaymentMethod paymentMethod;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------



}
