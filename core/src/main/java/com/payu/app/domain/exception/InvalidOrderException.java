package com.payu.app.domain.exception;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class InvalidOrderException extends RuntimeException {

    public InvalidOrderException(String message) {
        super(message);
    }

}