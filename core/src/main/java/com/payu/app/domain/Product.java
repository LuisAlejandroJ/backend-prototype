package com.payu.app.domain;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Product {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    // UUID
    private String id;

    private String name;

    private String category;

    // Double ?????
    private Integer price;

    // Integer????
    private Integer stock;

    private Integer quantity;

    private String imageUrl;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    // true or false?
    public void validate() {

    }

    public static Boolean isValid(Product product) {
        return false;
    }

    // reduce stock
    public void reduceStock() throws Exception {
        var newStock = stock - quantity;
        if (newStock >= 0) {
            this.stock = newStock;
            this.quantity = 0;
        } else {
            throw new Exception("Not enough products.");
        }

    }

}
