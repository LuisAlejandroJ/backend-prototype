package com.payu.app.domain.exception;

import com.payu.app.domain.enumTypes.OrderStatus;

public class InvalidOrderStatusException extends RuntimeException {

    public InvalidOrderStatusException(OrderStatus currentStatus, OrderStatus newStatus) {
        super(String.format("Unable to change order status, Message: You can't move from [%s] to [%s]", currentStatus, newStatus));
    }

}