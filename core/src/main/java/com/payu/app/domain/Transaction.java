package com.payu.app.domain;

import com.payu.app.domain.enumTypes.TransactionStatus;
import com.payu.app.domain.enumTypes.TransactionType;
import com.payu.app.domain.exception.InvalidTransactionException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class Transaction {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    // UUID
    private String id;

    private TransactionStatus status;

    private TransactionType type;

    private User payer;

    private PaymentCard paymentCard;

    // PayU Order Id
    private String paymentEngineOrderId;

    private String paymentEngineTransactionId;

    private String paymentEngineParentTransactionId;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Validate if a transaction with type {@link TransactionType#REFUND} has
     * associated a paymentEngineParentTransactionId, otherwise a
     * {@link InvalidTransactionException} is gonna be thrown.
     *
     */
    public void isTypeValid(){

        var isTypeRefund = type == TransactionType.REFUND;
        var parenTransactionIsNull = paymentEngineParentTransactionId == null;
        if(isTypeRefund && parenTransactionIsNull){
            var message = String.format("Invalid Transaction - Message: No Parent transaction found. Transaction if: [%s]", paymentEngineTransactionId);
            throw new InvalidTransactionException(message);        }
    }

}
