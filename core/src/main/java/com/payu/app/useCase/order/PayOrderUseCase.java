package com.payu.app.useCase.order;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.Order;
import com.payu.app.domain.TokenizeCard;
import com.payu.app.domain.Transaction;
import com.payu.app.domain.enumTypes.OrderStatus;
import com.payu.app.domain.enumTypes.TransactionStatus;
import com.payu.app.domain.exception.InvalidTransactionException;
import com.payu.app.repository.OrderRepository;
import com.payu.app.repository.PaymentEngineAdapter;
import com.payu.app.repository.TransactionRepository;
import com.payu.app.useCase.order.exception.InvalidPaymentException;
import com.payu.app.useCase.tokenizedCard.TokenizeCardUseCase;
import lombok.extern.slf4j.Slf4j;

/**
 * Author: Luis Alejandro Jaramillo
 */
@Slf4j
public class PayOrderUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private PaymentEngineAdapter paymentsAdapter;
    private OrderRepository orderRepository;
    private TransactionRepository transactionsRepository;
    private TokenizeCardUseCase tokenizeCardUseCase;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link PayOrderUseCase} with {@link CreditCard}
     *
     * @param order      {@link Order} object
     * @param creditCard the {@link CreditCard} witch it will be used
     * @return if order is created and the payment engine responds successfully, then
     * a {@link Order} with the new order instance is gonna be returned, otherwise a
     * {@link InvalidPaymentException} is gonna be thrown.
     */
    public Order execute(Order order, CreditCard creditCard) {

        orderRepository.add(order);

        if (creditCard.getSaveCard()) {
            tokenizeCardUseCase.execute(creditCard, order.getBuyer());
        }

        var transaction = paymentsAdapter.pay(order, creditCard)
                .orElseThrow(() -> {
                    var message = String.format("Message: Payment could not be completed. Order id: [%s]", order.getId());
                    throw new InvalidPaymentException(message);
                });
        addTransaction(transaction);

        return updateOrder(order, transaction);
    }

    /**
     * Execute the {@link PayOrderUseCase} with {@link TokenizeCard}
     *
     * @param order        {@link Order} object
     * @param tokenizeCard the {@link TokenizeCard} witch it will be used
     * @return if order is created and the payment engine responds successfully, then
     * a {@link Order} with the new order instance is gonna be returned, otherwise a
     * {@link InvalidPaymentException} is gonna be thrown.
     */
    public Order execute(Order order, TokenizeCard tokenizeCard) {

        orderRepository.add(order);

        var transaction = paymentsAdapter.pay(order, tokenizeCard)
                .orElseThrow(() -> {
                    var message = String.format("Message: Payment could not be completed. Order id: [%s]", order.getId());
                    throw new InvalidPaymentException(message);
                });

        addTransaction(transaction);

        return updateOrder(order, transaction);
    }

    /**
     * Add a {@link Transaction} using the {@link TransactionRepository}, if the {@link Transaction}
     * is created nothing will returned, otherwise a {@link InvalidTransactionException} is
     * gonna be thrown.
     *
     * @param transaction {@link Transaction} object
     */
    private void addTransaction(Transaction transaction) {

        transactionsRepository.add(transaction)
                .orElseThrow(() -> {
                    var message = String.format("Transaction could not be created. Transaction id: [%s]", transaction.getId());
                    throw new InvalidTransactionException(message);
                });
    }

    /**
     * @param order       order {@link Order} object
     * @param transaction the new {@link Transaction} associated with the payment
     * @return a {@link Order} instance with the new status and the new transaction
     */
    private Order updateOrder(Order order, Transaction transaction) {

        order.addTransaction(transaction);
        if (transaction.getStatus() == TransactionStatus.APPROVED) {
            order.changeStatus(OrderStatus.PAYED);
        } else {
            order.changeStatus(OrderStatus.UNDERPAYMENT);
        }
        orderRepository.update(order.getId(), order);
        return order;
    }

}
