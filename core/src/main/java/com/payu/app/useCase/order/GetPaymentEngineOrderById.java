package com.payu.app.useCase.order;

import com.payu.app.domain.Order;
import com.payu.app.repository.PaymentEngineAdapter;
import com.payu.app.useCase.order.exception.OrderNotFoundException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetPaymentEngineOrderById {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private PaymentEngineAdapter paymentsAdapter;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    public Order execute(String orderId) {
        return paymentsAdapter.consultOrder(orderId)
                .orElseThrow(() -> {
                    var message = String.format("Order not found in Payment Engine Order Id: [%s]", orderId);
                    throw new OrderNotFoundException(message);
                });
    }

}
