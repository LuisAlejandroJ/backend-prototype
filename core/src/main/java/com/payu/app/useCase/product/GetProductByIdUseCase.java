package com.payu.app.useCase.product;

import com.payu.app.domain.Product;
import com.payu.app.repository.ProductRepository;
import com.payu.app.useCase.product.exception.ProductNotFoundException;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetProductByIdUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private ProductRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetProductByIdUseCase}
     *
     * @param id the desired {@link Product} identifier
     * @return if product is found, then a Optional {@link Product} instance is gonna be returned, otherwise
     * a {@link ProductNotFoundException} is gonna be thrown
     */
    public Optional<Product> execute(String id) {
        return Optional.of(repository.getById(id)
                .orElseThrow(() -> {
                    var message = String.format("Product not found. payment id: [%s]", id);
                    throw new ProductNotFoundException(message);
                })
        );
    }
}
