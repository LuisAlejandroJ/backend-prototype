package com.payu.app.useCase.order.exception;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(String message) {
        super(message);
    }

}