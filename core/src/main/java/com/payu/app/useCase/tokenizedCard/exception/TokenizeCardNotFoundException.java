package com.payu.app.useCase.tokenizedCard.exception;

public class TokenizeCardNotFoundException extends RuntimeException{
    public TokenizeCardNotFoundException(String message) {
        super(message);
    }
}