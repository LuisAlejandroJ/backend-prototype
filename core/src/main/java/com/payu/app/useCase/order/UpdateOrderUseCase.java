package com.payu.app.useCase.order;

import com.payu.app.domain.Order;
import com.payu.app.repository.OrderRepository;
import com.payu.app.useCase.order.exception.OrderNotFoundException;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class UpdateOrderUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private OrderRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link UpdateOrderUseCase}
     *
     * @param id       the desired {@link Order} identifier
     * @param newOrder the new {@link Order}
     * @return if order is found, then a Optional {@link Order} instance that was update is gonna be returned,
     * otherwise a {@link RuntimeException} is gonna be thrown
     */
    public Optional<Order> execute(String id, Order newOrder) {

        return repository.getById(id)
                .map(oldOrder -> repository.update(oldOrder.getId(), newOrder))
                .orElseThrow(() -> {
                            var message = String.format("Order not found. Order id: [%s]", id);
                            throw new RuntimeException(message);
                        }
                );
    }

}
