package com.payu.app.useCase.order;

import com.payu.app.domain.Order;
import com.payu.app.domain.Transaction;
import com.payu.app.domain.enumTypes.OrderStatus;
import com.payu.app.domain.enumTypes.TransactionStatus;
import com.payu.app.domain.exception.InvalidTransactionException;
import com.payu.app.repository.OrderRepository;
import com.payu.app.repository.PaymentEngineAdapter;
import com.payu.app.repository.TransactionRepository;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class RefundOrderUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    PaymentEngineAdapter paymentsAdapter;

    TransactionRepository transactionRepository;

    OrderRepository orderRepository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    public Order execute(Order order, Transaction transaction){

        var newTransaction =  paymentsAdapter.refundOrder(transaction)
                .orElseThrow(() -> {
                    throw new RuntimeException("");
                });

        addTransaction(newTransaction);

        return updateOrder(order, transaction);

    }

    /**
     * Add a {@link Transaction} using the {@link TransactionRepository}, if the {@link Transaction}
     * is created nothing will returned, otherwise a {@link InvalidTransactionException} is
     * gonna be thrown.
     *
     * @param transaction {@link Transaction} object
     */
    private void addTransaction(Transaction transaction) {

        transactionRepository.add(transaction)
                .orElseThrow(() -> {
                    var message = String.format("Transaction could not be created. Transaction id: [%s]", transaction.getId());
                    throw new InvalidTransactionException(message);
                });
    }

    /**
     * @param order       order {@link Order} object
     * @param transaction the new {@link Transaction} associated with the payment
     * @return a {@link Order} instance with the new status and the new transaction
     */
    private Order updateOrder(Order order, Transaction transaction) {

        order.addTransaction(transaction);
        if (transaction.getStatus() == TransactionStatus.APPROVED) {
            order.changeStatus(OrderStatus.REFUNDED);
        } else {
            order.changeStatus(OrderStatus.SHIPPING);
        }
        orderRepository.update(order.getId(), order);
        return order;
    }
}
