package com.payu.app.useCase.user;

import com.payu.app.domain.User;
import com.payu.app.repository.UserRepository;
import com.payu.app.useCase.user.exception.UserNotFoundException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetUserByEmailUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private UserRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetUserByEmailUseCase}
     *
     * @param email the desired {@link User} email (identifier)
     * @return if product is found, then a Optional {@link User} instance is gonna be returned, otherwise
     * a {@link UserNotFoundException} is gonna be thrown
     */
    public User execute(String email) {
        return repository.getUserByEmail(email)
                .orElseThrow(() -> {
                    var message = String.format("User not found, with Email: [%s]", email);
                    throw new UserNotFoundException(message);
                });
    }
}
