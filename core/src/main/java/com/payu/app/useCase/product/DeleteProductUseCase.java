package com.payu.app.useCase.product;

import com.payu.app.domain.Product;
import com.payu.app.repository.ProductRepository;
import com.payu.app.useCase.product.exception.ProductNotFoundException;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class DeleteProductUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private ProductRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link DeleteProductUseCase}
     *
     * @param id the desired {@link Product} identifier
     * @return if product is found, then a Optional {@link Product} instance is gonna be returned and the product
     * is gonna be deleted, otherwise a {@link ProductNotFoundException} is gonna be thrown
     */
    public Optional<Product> execute(String id){
        var productFound = repository.getById(id);
        if(productFound.isPresent()){
            return repository.delete(id);
        }else{
            var message = String.format("Product not found. Product id: [%s]", id);
            throw new ProductNotFoundException(message);
        }
    }
}
