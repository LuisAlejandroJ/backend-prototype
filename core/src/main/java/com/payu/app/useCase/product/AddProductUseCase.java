package com.payu.app.useCase.product;

import com.payu.app.domain.Product;
import com.payu.app.repository.ProductRepository;
import com.payu.app.useCase.product.exception.ProductNotFoundException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class AddProductUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private ProductRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link AddProductUseCase}
     *
     * @param product {@link Product} object
     * @return if product is created, then a Optional {@link Product} instance is gonna be returned, otherwise a
     * {@link ProductNotFoundException} is gonna be thrown.
     */
    public Product execute(Product product) {
        return repository.add(product)
                .orElseThrow(() -> {
                    var message = String.format("Product could not be created. Product id: [%s]", product.getId());
                    throw new ProductNotFoundException(message);
                });
    }
}
