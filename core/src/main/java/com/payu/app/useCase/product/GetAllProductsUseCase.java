package com.payu.app.useCase.product;

import com.payu.app.domain.Product;
import com.payu.app.repository.ProductRepository;
import com.payu.app.useCase.product.exception.ProductNotFoundException;

import java.util.Collection;


/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetAllProductsUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private ProductRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetAllProductsUseCase}
     *
     * @return a Collection of all the {@link Product}
     */
    public Collection<Product> execute(){
        return repository.getAll();
    }
}
