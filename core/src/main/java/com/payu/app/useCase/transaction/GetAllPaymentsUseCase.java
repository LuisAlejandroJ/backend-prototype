package com.payu.app.useCase.transaction;

import com.payu.app.domain.Transaction;
import com.payu.app.repository.TransactionRepository;

import java.util.Collection;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetAllPaymentsUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private TransactionRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetAllPaymentsUseCase}
     *
     * @return a Collection of all the {@link Transaction}
     */
    public Collection<Transaction> execute(){
        return repository.getAll();
    }
}
