package com.payu.app.useCase.tokenizedCard;

import com.payu.app.domain.CreditCard;
import com.payu.app.domain.TokenizeCard;
import com.payu.app.domain.User;
import com.payu.app.repository.PaymentEngineAdapter;
import com.payu.app.repository.TokenizeCardRepository;
import com.payu.app.useCase.tokenizedCard.exception.TokenizeCardNotFoundException;
import com.payu.app.useCase.tokenizedCard.exception.TokenizeCardNotReceived;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class TokenizeCardUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private PaymentEngineAdapter paymentsAdapter;

    private TokenizeCardRepository tokenizeCardRepository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link TokenizeCardUseCase}
     *
     * @param creditCard {@link TokenizeCard} object to de added
     * @return if the {@link TokenizeCard} is saved into the payment engine and added
     * into the repository, then a {@link TokenizeCard} instance is gonna be returned
     * otherwise a {@link TokenizeCardNotReceived} or a {@link TokenizeCardNotFoundException}
     * would be thrown.
     */
    public TokenizeCard execute(CreditCard creditCard, User user) {

        var tokenizedCard = paymentsAdapter.tokenizeCard(creditCard,user);

        if(tokenizedCard.isPresent()) {
            tokenizeCardRepository.add(creditCard).orElseThrow(() -> {
                var message = String.format("Tokenize card could not be created. Owner of the card: [%s] ",user.getEmail());
                throw new TokenizeCardNotFoundException(message);
            });
            return tokenizedCard.get();
        }else {
            var message = String.format("Tokenize card was not received by the payment engine. Owner of the card: [%s] ",user.getEmail());
            throw new TokenizeCardNotReceived(message);
        }
    }

}
