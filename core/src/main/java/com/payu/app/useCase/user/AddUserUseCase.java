package com.payu.app.useCase.user;

import com.payu.app.domain.User;
import com.payu.app.repository.UserRepository;
import com.payu.app.useCase.user.exception.UserNotFoundException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class AddUserUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private UserRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link AddUserUseCase}
     *
     * @param user {@link User} object
     * @return if user is created, then a {@link User} instance is gonna be returned, otherwise a
     * {@link UserNotFoundException} is gonna be thrown.
     */
    public User execute(User user) {

        return repository.add(user)
                .orElseThrow(() -> {
                    var message = String.format("User could not be created. User email: [%s]", user.getEmail());
                    throw new UserNotFoundException(message);
                });
    }

}
