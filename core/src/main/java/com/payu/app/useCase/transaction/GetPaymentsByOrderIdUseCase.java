package com.payu.app.useCase.transaction;

import com.payu.app.domain.Order;
import com.payu.app.domain.Transaction;
import com.payu.app.repository.TransactionRepository;
import com.payu.app.useCase.transaction.exception.PaymentNotFoundException;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetPaymentsByOrderIdUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private TransactionRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetPaymentsByOrderIdUseCase}
     *
     * @param id the desired {@link Order} identifier
     * @return if payment is found, then a {@link Transaction} instance is gonna be returned, otherwise
     * a {@link PaymentNotFoundException} is gonna be thrown
     */
    public Transaction execute(String id) {
        return repository.getByOrderId(id)
                .orElseThrow(() -> {
                    var message = String.format("Payment not found. Order id: [%s]", id);
                    throw new PaymentNotFoundException(message);
                });
    }
}
