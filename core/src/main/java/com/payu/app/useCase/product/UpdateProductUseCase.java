package com.payu.app.useCase.product;

import com.payu.app.domain.Product;
import com.payu.app.repository.ProductRepository;
import com.payu.app.useCase.product.exception.ProductNotFoundException;

import java.util.Optional;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class UpdateProductUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private ProductRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link DeleteProductUseCase}
     *
     * @param id the desired {@link Product} identifier
     * @param newProduct {@link Product} object
     * @return if product is found, then a Optional {@link Product} instance is gonna be returned and the product
     * is gonna be update, otherwise a {@link ProductNotFoundException} is gonna be thrown
     */
    public Optional<Product> execute(String id, Product newProduct){
        var productFound = repository.getById(id);
        if(productFound.isPresent()){
            return repository.update(id,newProduct);
        }else{
            var message = String.format("Product not found. Product id: [%s]", id);
            throw new ProductNotFoundException(message);
        }
    }
}
