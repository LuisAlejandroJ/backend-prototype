package com.payu.app.useCase.tokenizedCard.exception;

public class TokenizeCardNotReceived extends RuntimeException {
    public TokenizeCardNotReceived(String message) {
        super(message);
    }
}
