package com.payu.app.useCase.order;

import com.payu.app.domain.Order;
import com.payu.app.domain.User;
import com.payu.app.repository.OrderRepository;

import java.util.Collection;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetOrdersByEmailUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private OrderRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetOrdersByEmailUseCase}
     *
     * @param email of the {@link User} object
     * @return Collection of {@link Order} of the {@link User}
     */
    public Collection<Order> execute(String email){
        return repository.getByEmail(email);
    }
}
