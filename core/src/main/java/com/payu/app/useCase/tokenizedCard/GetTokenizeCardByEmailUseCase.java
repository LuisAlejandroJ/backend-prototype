package com.payu.app.useCase.tokenizedCard;

import com.payu.app.domain.TokenizeCard;
import com.payu.app.domain.User;
import com.payu.app.repository.TokenizeCardRepository;

import java.util.Collection;

/**
 * Author: Luis Alejandro Jaramillo
 */
public class GetTokenizeCardByEmailUseCase {

    // -----------------------------------------------------------------------------------------------------------------
    // Structure
    // -----------------------------------------------------------------------------------------------------------------

    private TokenizeCardRepository repository;

    // -----------------------------------------------------------------------------------------------------------------
    // Behaviour
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Execute the {@link GetTokenizeCardByEmailUseCase}
     *
     * @param email of the {@link User} owner or which registered the  card
     * @return if card is found, then a Collection of {@link TokenizeCard} is gonna be returned, otherwise
     * a empty collection.
     */
    public Collection<TokenizeCard> execute(String email){
        return repository.getByEmail(email);
    }
}
